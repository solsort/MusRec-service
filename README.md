Anbefalings-webservice for SMK-billeder
=======================================

Når servicen kører, så kan anbefalinger hentes på:

> http://localhost:8000/?id=...&img=...&limit=...

eksempelvis:

> http://localhost:8000/?id=KMS1&img=https%3A%2F%2Fiip.smk.dk%2Fiiif%2Fjp2%2FKMS1-cropped.tif.jp2%2Ffull%2F!3840%2C%2F0%2Fdefault.jpg&limit=100

`id`-parameteren er identifikationen af billedet.  Hvis billedet allerede finde i anbefalingssystemet, - finder den blot anbefalinger ud fra `id`.  Hvis `id` er tomt, eller endnu ikke eksisterer i anbefalingssystemet, så henter servicen billedet fra urlen angivet i `img`-parameteren, og udregner derefter anbefalinger, - og hvis `id` er angivet så vil det blive tilføjet til anbefalingssystemt og bruges til fremtidige anbefalinger. Parameteren `limit` angiver hvor mange anbefalinger der skal returneres.

Kildekode og datamodel ligger på https://gitlab.com/solsort/MusRec-service og indeholder også `Dockerfile`.
