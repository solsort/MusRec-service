FROM python:3
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
VOLUME ["/app/data"]
CMD ["gunicorn", "--bind=0.0.0.0", "-w", "1", "app:app"]
EXPOSE 8000
