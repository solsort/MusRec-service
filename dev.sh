(sleep 1; touch app.py) &
while inotifywait -e modify,close_write,move_self -q *.py
do 
  kill -9 `cat .pid`
  sleep 0.3
  clear;
  python app.py  &
  echo $! > .pid
  sleep 4
  curl -v "http://localhost:5000/"
  curl "http://localhost:5000/?id=KMS1&img=https%3A%2F%2Fiip.smk.dk%2Fiiif%2Fjp2%2FKMS1-cropped.tif.jp2%2Ffull%2F!3840%2C%2F0%2Fdefault.jpg&limit=10"
  curl -v "http://localhost:5000/?id=KMSsp491&img=https%3A%2F%2Fiip.smk.dk%2Fiiif%2Fjp2%2Fkmssp491.tif.reconstructed.tif.jp2%2Ffull%2F!1600%2C%2F0%2Fdefault.jpg&limit=10"
done
