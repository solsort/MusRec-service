from fastai.vision import load_learner, open_image
from urllib.request import urlretrieve
import numpy as np
import pickle
import os
from flask import Flask, request, jsonify

os.system('mkdir -p data; cp -n ids.pkl weights.npy data/')
learn = load_learner('model')
learn.to_fp16()
with open("data/ids.pkl", 'rb') as f:
    ids = pickle.load(f)
# ids = [s.upper() for s in ids]
m = np.load("data/weights.npy")

cntr = 0
def recommend(id, url, limit):
    global cntr, m;
    
    v = None
    # id = id.upper()
    try:
        idx = ids.index(id)
        v = m[idx]
    except:
        tmpname = 'imgs/' + str(cntr)
        cntr = cntr + 1
        img = None
        try:
            urlretrieve(url, tmpname)
            img = open_image(tmpname)
        except:
            os.system("rm -f " + tmpname)
            return {"error": "could not load image"}
        pred_class,pred_idx,outputs = learn.predict(img)                
        v = np.array(outputs)
        if id:
            ids.append(id)
            m = np.append(m, [v], axis=0)
            with open("data/ids.pkl", 'wb') as f:
                pickle.dump(ids, f)
            np.save('data/weights.npy', m)
        os.system("rm " + tmpname)
    t = m - v
    t = np.abs(t)
    t = np.argsort(np.sum(t, axis=1))
    return [ids[i] for i in t[0:limit]]


with open('README.md', 'r') as f:
    info = f.read()


app = Flask(__name__)

@app.route('/')
def main():
    args = request.args
    limit = int(args.get('limit', 10))
    id = args.get('id', '')
    img = args.get('img', '')
    if not id and not img:
        return info, 200, {"Content-Type": "text/markdown"}
    return jsonify(recommend(id, img, limit))



if __name__ == '__main__':
    app.run(host='0.0.0.0')
